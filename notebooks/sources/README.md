# Data sources

These notebooks serve us to practice some data wrangling techniques, and as snippets to build on. 

- [CSV file headers](csv_headers.ipynb)
- [CSV delimiters](csv_delimiters.ipynb)
- [Skiprows and nrows](skipping.ipynb)
- [Reading CSV data from a compressed file](reading_compressed.ipynb)
- [Reading sheets from an Excel file](reading_excel.ipynb)
- [Reading a Text file](reading_txt.ipynb)
- [Reading HTML tables](reading_html.ipynb)
- [Reading from a JSON file](reading_json.ipynb)
- [Reading a PDF file](reading_pdf.ipynb)
- [Using Beautiful Soup](bs4.ipynb)
- [Exporting a DataFrame as an Excel file](export_excel.ipynb)
