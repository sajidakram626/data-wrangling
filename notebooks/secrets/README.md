# Some secrets

These notebooks serve us to practice some data wrangling techniques, and as snippets to build on. 

- [Generator expressions](generator-expressions.ipynb)
- [Zip function and messy data](zip-function.ipynb)
- [Data representation using {}](curly-brackets.ipynb)
- [Outliers in numerical data](outliers.ipynb)
- [Rinse and repeat](rinse-repeat.ipynb)