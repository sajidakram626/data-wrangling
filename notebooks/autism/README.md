# Autism

Using the [Autistic Spectrum Disorder Screening Data for Children](https://archive-beta.ics.uci.edu/ml/datasets/autistic+spectrum+disorder+screening+data+for+children) of 2017 for going through all the steps to solve a problem and building an interactive comprehensive program people can use effectively for predicting autism in children.
