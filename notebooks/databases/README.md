# RDBMS and SQL

These notebooks serve us to practice some data wrangling techniques, and as snippets to build on. 

- [Discworld database and witches table](witches.ipynb)
- [Quotes table](quotes.ipynb)
- [Joining and creating dataframe](dataframe.ipynb)