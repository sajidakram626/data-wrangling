# Titanic

Exploratory Data Analysis (EDA) of the titanic dataset.

## RMS Titanic and dataset
The RMS Titanic was known as the unsinkable ship and was the largest, most luxurious passenger ship of its time. Sadly, the British ocean liner sank on April 15, 1912, killing over 1500 people while just 705 survived.

We explored the domain more on the [Encyclopedia Titanica](https://www.encyclopedia-titanica.org/) and are using the [original titanic dataset](https://github.com/alexisperrier/packt-aml/blob/master/ch4/original_titanic.csv): 
The data consists of demographic and traveling information for 1,309 of the Titanic passengers, and the goal is to predict the survival of these passengers.

## Notebooks
- [Exploring the Titanic dataset](initial_exploration.ipynb)
- [Cleaning the Titanic dataset](cleaning.ipynb)
- [Feature engineering Titanic dataset](feature-engineering.ipynb)
- [Choosing model(s) for the Titanic dataset](choose_model.ipynb)

