# Data gathering

These notebooks serve us to practice some data wrangling techniques, and as snippets to build on. 

- [Using requests and beautiful soup](requests.ipynb)
- [Reading data from XML](xml.ipynb)
- [Reading data from an API](api.ipynb)
- [Gutenberg](gutenberg.ipynb)