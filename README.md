# Data wrangling

### Learning
- [x] [Diving in](notebooks/diving)
- [x] [Data sources](notebooks/sources)
- [x] [Some secrets](notebooks/secrets)
- [x] [Data gathering](notebooks/scraping)
- [x] [RDBMS and SQL](notebooks/databases)

### Applications
...

## Requirements

* anaconda3 with python 3.9 interpreter
* or pyenv and poetry
* or venv and pip-tools

## Installation
* [install anaconda](https://www.anaconda.com/products/individual) with jupyter notebook.
* Type annotations and `mypy`: Install `data-science-types` for libraries like matplotlib, numpy and pandas that do not have type information.

## Project status
Back to Square One.

## Resources

### Books
* ["Data Cleaning" by Ihab Ilyas and Xu Chu, 2019](https://dl.acm.org/doi/book/10.1145/3310205) - Data quality is one of the most important problems in data management, since dirty data often leads to inaccurate data analytics results and incorrect decisions.

### Workshops
* [Kaggle Data Cleaning tutorial](https://www.kaggle.com/learn/data-cleaning) - Master efficient workflows for cleaning real-world, messy data.
* [The Data Wrangling Workshop](https://github.com/PacktWorkshops/The-Data-Wrangling-Workshop) - Two years old and some code is not working, but that which we used to practice with and learn from and didn't work was corrected.

### Datasets
* [CORGIS: JSON Datasets](https://corgis-edu.github.io/corgis/json/)
* [UC Irvine Machine Learning Repository](https://archive-beta.ics.uci.edu/)
* [University of Texas Inequality Project](https://utip.lbj.utexas.edu/datasets.html)
* [Data Science Dojo](https://code.datasciencedojo.com/datasciencedojo/datasets)

## Contributing
This project welcomes contributions.

## License
Unlicensed. Or Universal Licensed. Whatever. This is in our playground.
